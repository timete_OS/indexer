Basic Web Indexer
=====================

## Goal

Trying to make a search engine based on some URLs.

## How it's going

It's hard.

## How to use

First run ```pip install -r requirements.txt``` in order to install dependecies.

### Create the index

If this is the first time you are using the program, you'll need to create the index. Just run 
```sh
python main.py --build crawled_urls.json  "ma premiere requête"
```

It will read urls from the **crawled_urls.json** file, requests them and forge a reversed index based on their content. Careful, the resulting file will make around 15 M and it will take around 3 minutes to compute (that is because of poor optimisation, and because I only used pure python. Nevermind.)

After this, you can just make requests like so :


```sh
you@YourLaptop:~/Documents/indexer$ python main.py  "poule de combat"
Il y a eu 141 résultats sur 500 possibilitées

1 -	https://qrm.fr/
	avec 1358 mentions dans le document

2 -	https://defenseurdesdroits.fr/fr/claire-hedon-defenseure-des-droits
	avec 1085 mentions dans le document

3 -	http://mael.monnier.free.fr/bac_francais/etranger/viecamus.htm
	avec 457 mentions dans le document

4 -	https://www.apple.com/fr/imovie/
	avec 440 mentions dans le document

5 -	https://www.frenchbee.com/fr
	avec 434 mentions dans le document

6 -	https://fr.wikipedia.org/wiki/Gu%C3%A9delon
	avec 426 mentions dans le document

7 -	https://www.mazeau-immobilier.com/
	avec 414 mentions dans le document

8 -	http://www.domaine-de-montine.com/
	avec 387 mentions dans le document

9 -	https://mjp.univ-perp.fr/constit/ly1977.htm
	avec 326 mentions dans le document

10 -	https://www.degriffelectromenager.com/
	avec 290 mentions dans le document

```

## How to run the tests

Just run ```python -m unittest``` from the project root directory

## How the engine works

For now, it doesn't do anything fancy. However, the project have been build to make easy any switch in the tokenizer or ranking function. It is supposedly possible to do more advanced work without building everything again.

For now, the engine give first the results of an AND search, then thoe of an OR search

## Future

None