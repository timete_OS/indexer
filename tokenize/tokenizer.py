import asyncio
import string

from lxml import html, etree, objectify


async def async_default(document):
    if document == '':
        return []
    try:
        tree = html.fromstring(document)
    except TypeError:
        print(f"le contenu n'est pas parsable :\n\n{document}")
        return []

    titles_and_paragraphs = tree.xpath("//*[self::p or self::title]")
    list_of_titles_and_paragraphs = [elm.text_content()
                                        .translate(str.maketrans('', '', string.punctuation))
                                        .split()
                                        for elm in titles_and_paragraphs]

    # flattening the list
    flat_list = [
        item for sublist in list_of_titles_and_paragraphs for item in sublist]
    return flat_list

def default(document):
    if document == '':
        return []
    try:
        tree = html.fromstring(document)
    except TypeError:
        print(f"le contenu n'est pas parsable :\n\n{document}")
        return []

    titles_and_paragraphs = tree.xpath("//*[self::p or self::title]")
    list_of_titles_and_paragraphs = [elm.text_content()
                                        .translate(str.maketrans('', '', string.punctuation))
                                        .split()
                                        for elm in titles_and_paragraphs]

    # flattening the list
    flat_list = [
        item for sublist in list_of_titles_and_paragraphs for item in sublist]
    return flat_list