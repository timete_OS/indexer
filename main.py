import argparse

import ranking.ranker as ranker
import tokenize.tokenizer as tokenizer

from index.index import Index


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Process some integers.')


    parser.add_argument('request', type=str,
                        help='request to the engine')
    parser.add_argument('--build', type=str, required=False, nargs='?',
                        help='filename to create the index')
    args = parser.parse_args()


    args = vars(args)


    filename = args.get('build')
    if filename:
        ### creating index files
        # ==================
        index = Index(filename)
        tokenized_content, tokens = index.create_tokens()
        index_table = index.create_index(tokenized_content, tokens)

        stats = {
            "token number" : len(tokens),
            "document number" : len(tokenized_content),
            "mean token peer doc" : round(sum([len(doc) for doc in tokenized_content])/len(tokenized_content), 2)
        }

        # Writing to file
        index.to_file(index_table, "title.pos_index.json")
        index.to_file(stats, "metadata.json")
    else:
        index = Index("crawled_urls.json")
        index_table = index.load("title.pos_index.json")

    ### answering requests
    # ==================
    request = args.get('request')
    if request:

        tokenized_req = tokenizer.default(request)

        responses = ranker.default(tokenized_req, index_table)

    index.print_responses(responses)






