import unittest
import asyncio

from index.index import Index


class TestIndexer(unittest.TestCase):

    def test_tokenizer(self):
        index = Index()
        # url = "https://en.wikipedia.org/wiki/Search_engine_indexing"
        # do not use wikipedia, site not stable and behavior differs if logged in or not
        url = "https://oeis.org/"
        r = asyncio.run(index.tokenize(url))

        # soooooooo bad
        expected_list = ['The', 'OnLine', 'Encyclopedia', 'of', 'Integer', 'Sequences®', 'OEIS®', 'Hints', 'Welcome',
                         'Video', 'For', 'more', 'information', 'about', 'the', 'Encyclopedia', 'see', 'the', 'Welcome', 'page']
        self.assertListEqual(r[:len(expected_list)], expected_list)

    def test_load(self):
        # try pinging google.com first ?

        index = Index()
        url = "https://en.wikipedia.org/wiki/Search_engine_indexing"

        r = asyncio.run(index.make_request(url))
        self.assertTrue(isinstance(r, bytes))

        url = "bloubli bloubli"

        r = asyncio.run(index.make_request(url))
        self.assertEqual(r, '')

        url = ""

        r = asyncio.run(index.make_request(url))
        self.assertEqual(r, '')