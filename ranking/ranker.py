import numpy as np
from Levenshtein import distance


def nearest_neighbour(word, vocab):
    distances = [distance(i, word) for i in vocab]
    return vocab[np.argmin(distances)]


def default(tokenized_request, index_table):
    vocab = np.array(list(index_table.keys()))
    vocab.sort()

    tokenized_request = [nearest_neighbour(t, vocab) for t in tokenized_request]

    res = []
    for t in tokenized_request:
        docs = index_table.get(t)
        res.append(set([doc[0] for doc in docs]))


    documents = {}
    for i in tokenized_request:
        docs = index_table.get(i)
        for doc in docs:
            doc_index = doc[0]
            if doc_index not in documents.keys():
                documents[doc_index] =  {
                'tokens':[i],
                'token_ctn':0,
                'token_nbr':1
                }    
            ctn = documents[doc_index].get('token_count', 0)
            tokens = documents[doc_index].get('tokens', [])
            tokens.append(i)
            documents[doc_index] = {
                'tokens':tokens,
                'token_ctn':ctn + len(doc[1]),
                'token_nbr':len(set(tokens))
            }            

    # wiipédia 53

    # here we give 100 times more weight to the results that share all tokens
    docs = {k: v for k, v in sorted(documents.items(), reverse=True, key=lambda item: item[1]['token_nbr']*100 + item[1]['token_ctn'])}
    return docs