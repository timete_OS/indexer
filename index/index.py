import json
import httpx
import asyncio
import string
import ssl

# import numpy as np

from lxml import html, etree, objectify
import tokenize.tokenizer as tokenizer

# httpx._config.DEFAULT_CIPHERS += ":ALL:@SECLEVEL=1"


class Index:

    def __init__(self, filename) -> None:
        self.urls = self.load(filename)


    def load(self, filename) -> list:
        with open(filename) as file:
            urls = json.load(file)
        return urls


    async def make_request(self, url: str) -> str:
        async with httpx.AsyncClient() as client:
            try:
                # can seriously fuck up here
                r = await client.get(url)
                if r.status_code == 200:
                    return r.content
                return ''
            except httpx.UnsupportedProtocol:
                return ''
            except httpx.ConnectTimeout:
                return ''
            except httpx.ConnectError:
                print(f"cannot resolve {url}")
                return ''
            except ssl.SSLCertVerificationError:
                # here dealing with self-signed certificate
                # we don't want to index malicious websites anyway
                print(f"{url} has signed itself its certificate")
                return ''
            except ValueError:
                print(f"{url} is not resolved to a valid IP address")
                return ''
            except Exception as err:
                print(f"Unexpected {err=}, {type(err)=}")

    async def tokenize(self, url: str) -> list:
        """tokenize a web page

        Args:
            url (str): url of the wanted web page

        Returns:
            list: all tokens in the page
        """
        content = await self.make_request(url)
        return await tokenizer.async_default(content)

    async def load_and_tokenize(self, urls: list):
        tokenized_content = await asyncio.gather(*[self.tokenize(url) for url in urls])
        tokenized_content = [e for e in tokenized_content if e != []]
        return tokenized_content

    def get_indices(self, token_set: list, token):
        return tuple(index for index, content in enumerate(token_set) if content == token)

    def create_tokens(self):
        if len(self.urls) == 0:
            return [], set()
        tokenized_content = asyncio.run(self.load_and_tokenize(self.urls))
        tokens = set.union(*[set(content) for content in tokenized_content])
        return tokenized_content, tokens

    def create_index(self, tokenized_content, tokens):
        # candidate for most unreadable python one-liner of the year
        # index_table = {token : [(index, tuple(index for index, content in enumerate(token_set) if content == token)) for index, token_set in enumerate(tokenized_content) if token in token_set] for token in tokens}

        index_table = {
            token: [(index, self.get_indices(token_set, token))
                    for index, token_set in enumerate(tokenized_content) if token in token_set]
            for token in tokens
        }

        return index_table

    def to_file(self, index_table, filename):
        json_object = json.dumps(index_table, indent=4)
        with open(filename, "w") as outfile:
            outfile.write(json_object)

    def print_responses(self, responses):
        s = f"Il y a eu {len(responses)} résultats sur {len(self.urls)} possibilitées\n\n"
        for index, i in enumerate(list(responses.keys())[:10]):
            s += f"{index + 1} -\t{self.urls[i]}\n\tavec {responses[i]['token_ctn']} mentions dans le document\n\n"
        print(s)